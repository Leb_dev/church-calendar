package dev.cyberhatter.churchcalendar.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Parcelable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Objects;

import dev.cyberhatter.churchcalendar.Global.ChurchCalendarAPI;
import dev.cyberhatter.churchcalendar.Global.RetrofitClient;
import dev.cyberhatter.churchcalendar.MainActivity;
import dev.cyberhatter.churchcalendar.Models.DateInfo;
import dev.cyberhatter.churchcalendar.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class DayInfoListAdapter extends BaseAdapter {

    private CompositeDisposable compositeDisposable;
    private final MainActivity mainActivity;
    private SparseArray<DateInfo> dateInfos;
    private ChurchCalendarAPI churchCalendarAPI;

    public DayInfoListAdapter(MainActivity mainActivity, SparseArray<DateInfo> dateInfos) {
        this.mainActivity = mainActivity;
        if (dateInfos == null) {
            this.dateInfos = new SparseArray<>();
            for (int i = 0; i < getCount(); i++) {
                this.dateInfos.put(i, null);
            }
        } else {
            this.dateInfos = dateInfos;
        }
        compositeDisposable = new CompositeDisposable();
        Retrofit retrofit = RetrofitClient.getClient();
        churchCalendarAPI = retrofit.create(ChurchCalendarAPI.class);
    }

    @Override
    public int getCount() {
        return mainActivity.calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    @Override
    public Object getItem(int position) {
        return dateInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (dateInfos.valueAt(position) != null) {
            DateInfo dateInfo = (DateInfo) getItem(position);
            convertView = LayoutInflater.from(mainActivity).inflate(R.layout.list_item_celebrations, parent, false);
            ((TextView) convertView.findViewById(R.id.day)).setText((position + 1) + "");
            ((TextView) convertView.findViewById(R.id.dayOfWeek)).setText(dateInfo.getWeekday() + "");
            LinearLayout linearLayout = convertView.findViewById(R.id.celebrationsLayout);
            for (DateInfo.Celebration celebration : dateInfo.getCelebrations()) {
                TextView textView = (TextView) LayoutInflater.from(mainActivity).inflate(R.layout.item_celebration, linearLayout, false);
                textView.setText(celebration.getTitle());
                Drawable drawable = Objects.requireNonNull(mainActivity.getDrawable(R.drawable.ic_calendar)).mutate();
                switch (celebration.getColour()) {
                    default:
                        break;
                    case "green":
                        drawable.setColorFilter(Color.GREEN, PorterDuff.Mode.SRC_ATOP);
                        break;
                    case "violet":
                        drawable.setColorFilter(Color.argb(0xFF, 0xEE, 0x82, 0xEE), PorterDuff.Mode.SRC_ATOP);
                        break;
                    case "white":
                        drawable.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                        break;
                    case "red":
                        drawable.setColorFilter(Color.RED, PorterDuff.Mode.SRC_ATOP);
                        break;
                }
                textView.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, null, null, null);
                textView.setCompoundDrawablePadding((int) (mainActivity.getResources().getDisplayMetrics().density * 8));
                linearLayout.addView(textView);
            }
        } else {
            convertView = LayoutInflater.from(mainActivity).inflate(R.layout.item_loading, parent, false);
            ConnectivityManager cm = (ConnectivityManager) mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            if (netInfo != null && netInfo.isConnected()) {
                compositeDisposable.add(
                        churchCalendarAPI.getDateInfo(mainActivity.calendar.get(Calendar.YEAR), mainActivity.calendar.get(Calendar.MONTH) + 1, position + 1)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(
                                        dateInfo -> {
                                            dateInfos.setValueAt(position, dateInfo);
                                            notifyDataSetChanged();
                                        },
                                        throwable -> {
                                            //Toast.makeText(mainActivity, throwable.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                )
                );
            } else {
                Toast.makeText(mainActivity, mainActivity.getString(R.string.no_internet_connection), Toast.LENGTH_SHORT).show();
            }
        }
        return convertView;
    }

    public SparseArray<? extends Parcelable> getItems() {
        return dateInfos;
    }
}
