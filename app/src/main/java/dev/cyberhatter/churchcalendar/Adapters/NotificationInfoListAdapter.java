package dev.cyberhatter.churchcalendar.Adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import dev.cyberhatter.churchcalendar.Models.NotificationInfo;
import dev.cyberhatter.churchcalendar.NotificationDialogActivity;
import dev.cyberhatter.churchcalendar.NotificationsActivity;
import dev.cyberhatter.churchcalendar.R;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class NotificationInfoListAdapter extends BaseAdapter {

    NotificationsActivity notificationsActivity;
    ArrayList<NotificationInfo> notificationInfos;

    @Override
    public int getCount() {
        return notificationInfos.size();
    }

    @Override
    public Object getItem(int position) {
        return notificationInfos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return ((NotificationInfo) getItem(position)).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(notificationsActivity).inflate(R.layout.list_item_notifications, parent, false);
        }
        NotificationInfo notificationInfo = (NotificationInfo) getItem(position);
        ((TextView) convertView.findViewById(R.id.notificationText)).setText(notificationInfo.getText());
        Date date = new Date(notificationInfo.getDate());
        ((TextView) convertView.findViewById(R.id.notificationDate)).setText(new SimpleDateFormat("HH:mm dd-MMM-yyyy", Locale.getDefault()).format(date));
        convertView.findViewById(R.id.notificationEdit).setOnClickListener(v -> {
            Intent intent = new Intent(notificationsActivity, NotificationDialogActivity.class);
            intent.putExtra(NotificationsActivity.BUNDLE_NOTIFICATION_ID, notificationInfo.getId());
            intent.putExtra(NotificationsActivity.BUNDLE_NOTIFICATION_TEXT, notificationInfo.getText());
            intent.putExtra(NotificationsActivity.BUNDLE_NOTIFICATION_DATE, notificationInfo.getDate());
            notificationsActivity.startActivityForResult(intent, notificationsActivity.REQUEST_CODE_EDIT_NOTIFICATION);
        });
        convertView.findViewById(R.id.notificationDelete).setOnClickListener(v -> {
            notificationsActivity.dataBaseHelper.deleteNotification(notificationInfo.getId());
            notificationsActivity.setAdapter();
        });
        return convertView;
    }
}
