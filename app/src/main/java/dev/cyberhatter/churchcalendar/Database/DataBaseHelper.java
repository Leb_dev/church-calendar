package dev.cyberhatter.churchcalendar.Database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import java.util.ArrayList;
import java.util.Calendar;

import dev.cyberhatter.churchcalendar.Models.NotificationInfo;

public class DataBaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "church_calendar.db";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase sqLiteDatabase;

    private final String TABLE_NAME_NOTIFICATIONS = "Notificaitons";
    private final String KEY_NOTIFICATIONS_TEXT = "NotificationText";
    private final String KEY_NOTIFICATIONS_DATE = "NotificationDate";
    private final String KEY_NOTIFICATIONS_ID = "_id";


    @Override
    public SQLiteDatabase getWritableDatabase() {
        if (sqLiteDatabase == null || !sqLiteDatabase.isOpen()) {
            sqLiteDatabase = super.getWritableDatabase();
        }
        return sqLiteDatabase;
    }

    public DataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(
                    "create table " + TABLE_NAME_NOTIFICATIONS +
                            "(" +
                            KEY_NOTIFICATIONS_ID + " integer primary key," +
                            KEY_NOTIFICATIONS_TEXT + " text," +
                            KEY_NOTIFICATIONS_DATE + " long" +
                            ")"
            );
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public ArrayList<NotificationInfo> getAllNotificationInfos() {
        ArrayList<NotificationInfo> notificationInfos = new ArrayList<>();
        Cursor cursor = getWritableDatabase().rawQuery("select * from " + TABLE_NAME_NOTIFICATIONS, null);
        while (cursor.moveToNext()) {
            notificationInfos.add(new NotificationInfo(
                    cursor.getInt(cursor.getColumnIndex(KEY_NOTIFICATIONS_ID)),
                    cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATIONS_TEXT)),
                    cursor.getLong(cursor.getColumnIndex(KEY_NOTIFICATIONS_DATE))
            ));
        }
        cursor.close();
        return notificationInfos;
    }

    public NotificationInfo addNotification(NotificationInfo notificationInfo) {
        SQLiteStatement sqLiteStatement = getWritableDatabase().compileStatement(
                "insert into " + TABLE_NAME_NOTIFICATIONS + " (" +
                        KEY_NOTIFICATIONS_TEXT + ", " +
                        KEY_NOTIFICATIONS_DATE + ") " +
                        "values (?, ?)"
        );
        sqLiteStatement.bindString(1, notificationInfo.getText());
        sqLiteStatement.bindLong(2, notificationInfo.getDate());
        if (sqLiteStatement.executeInsert() == -1) {
            return null;
        }
        return notificationInfo;
    }

    public void deleteNotification(int id) {
        SQLiteStatement sqLiteStatement = getWritableDatabase().compileStatement(
                "delete from " + TABLE_NAME_NOTIFICATIONS + " " +
                        "where " + KEY_NOTIFICATIONS_ID + "=?"
        );
        sqLiteStatement.bindLong(1, id);
        sqLiteStatement.execute();
    }

    public void editNotification(NotificationInfo notificationInfo) {
        if (notificationInfo.getId() == -1) {
            addNotification(notificationInfo);
            return;
        }
        SQLiteStatement sqLiteStatement = getWritableDatabase().compileStatement(
                "update " + TABLE_NAME_NOTIFICATIONS + " " +
                        "set " + KEY_NOTIFICATIONS_TEXT + "=?, " +
                        KEY_NOTIFICATIONS_DATE + "=? " +
                        "where " + KEY_NOTIFICATIONS_ID + "=?"
        );
        sqLiteStatement.bindString(1, notificationInfo.getText());
        sqLiteStatement.bindLong(2, notificationInfo.getDate());
        sqLiteStatement.bindLong(3, notificationInfo.getId());
        sqLiteStatement.execute();
    }

    public ArrayList<NotificationInfo> getNextWakeTime() {
        ArrayList<NotificationInfo> notificationInfos = new ArrayList<>();
        Cursor cursor = getWritableDatabase().rawQuery("select * from " + TABLE_NAME_NOTIFICATIONS + " " +
                "where " + KEY_NOTIFICATIONS_DATE + ">? " +
                "order by " + KEY_NOTIFICATIONS_DATE + " asc", new String[]{Calendar.getInstance().getTimeInMillis() + ""});
        if (!cursor.moveToFirst()) {
            return notificationInfos;
        }
        long time = cursor.getLong(cursor.getColumnIndex(KEY_NOTIFICATIONS_DATE));
        do {
            if (cursor.getLong(cursor.getColumnIndex(KEY_NOTIFICATIONS_DATE)) != time) {
                break;
            }
            notificationInfos.add(new NotificationInfo(
                    cursor.getInt(cursor.getColumnIndex(KEY_NOTIFICATIONS_ID)),
                    cursor.getString(cursor.getColumnIndex(KEY_NOTIFICATIONS_TEXT)),
                    cursor.getLong(cursor.getColumnIndex(KEY_NOTIFICATIONS_DATE))
            ));
            cursor.moveToNext();
        } while (!cursor.isAfterLast());
        cursor.close();
        return notificationInfos;
    }

    public ArrayList<String> getNotifications(int[] ids) {
        ArrayList<String> arrayList = new ArrayList<>();
        for (int id : ids) {
            SQLiteStatement sqLiteStatement = getWritableDatabase().compileStatement(
                    "select " + KEY_NOTIFICATIONS_TEXT + " from " + TABLE_NAME_NOTIFICATIONS + " " +
                            "where " + KEY_NOTIFICATIONS_ID + "=?"
            );
            sqLiteStatement.bindLong(1, id);
            arrayList.add(sqLiteStatement.simpleQueryForString());
        }
        return arrayList;
    }
}
