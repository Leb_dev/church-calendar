package dev.cyberhatter.churchcalendar.Global;

import dev.cyberhatter.churchcalendar.Models.DateInfo;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

//http://calapi.inadiutorium.cz/
public interface ChurchCalendarAPI {
    @GET("{year}/{month}/{day}")
    Observable<DateInfo> getDateInfo(
            @Path("year") int year,
            @Path("month") int month,
            @Path("day") int day
    );
}
