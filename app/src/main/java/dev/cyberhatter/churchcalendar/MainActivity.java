package dev.cyberhatter.churchcalendar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.NumberPicker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import dev.cyberhatter.churchcalendar.Adapters.DayInfoListAdapter;

public class MainActivity extends AppCompatActivity {

    private static final String BUNDLE_CALENDAR = "calendar";
    private static final String BUNDLE_LIST = "list";
    @BindView(R.id.list)
    ListView listView;
    @BindView(R.id.yearPicker)
    NumberPicker yearPicker;
    @BindView(R.id.monthPicker)
    NumberPicker monthPicker;
    public Calendar calendar;
    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;
    int lastSeenItem = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        calendar = Calendar.getInstance();
        if (savedInstanceState != null) {
            calendar.setTimeInMillis(savedInstanceState.getLong(BUNDLE_CALENDAR));
        }
        yearPicker.setMinValue(1980);
        yearPicker.setMaxValue(9999);
        yearPicker.setValue(calendar.get(Calendar.YEAR));
        yearPicker.setWrapSelectorWheel(false);
        monthPicker.setMinValue(1);
        monthPicker.setMaxValue(12);
        monthPicker.setValue(calendar.get(Calendar.MONTH) + 1);
        NumberPicker.OnValueChangeListener onValueChangeListener = (picker, oldVal, newVal) -> setListViewAdapter();
        yearPicker.setOnValueChangedListener(onValueChangeListener);
        monthPicker.setOnValueChangedListener(onValueChangeListener);

        if (savedInstanceState == null) {
            listView.setAdapter(new DayInfoListAdapter(this, null));
        } else {
            listView.setAdapter(new DayInfoListAdapter(this, savedInstanceState.getSparseParcelableArray(BUNDLE_LIST)));
        }
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (firstVisibleItem == 0 || firstVisibleItem < lastSeenItem) {
                    lastSeenItem = firstVisibleItem;
                    floatingActionButton.show();
                } else {
                    if (firstVisibleItem > lastSeenItem) {
                        lastSeenItem = firstVisibleItem;
                        floatingActionButton.hide();
                    }
                }
            }
        });

        floatingActionButton.setOnClickListener(v -> startActivity(new Intent(MainActivity.this, NotificationsActivity.class)));
    }

    void setListViewAdapter() {
        calendar.set(yearPicker.getValue(), monthPicker.getValue() - 1, calendar.get(Calendar.DAY_OF_MONTH));
        listView.setAdapter(new DayInfoListAdapter(MainActivity.this, null));
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(BUNDLE_CALENDAR, calendar.getTimeInMillis());
        outState.putSparseParcelableArray(BUNDLE_LIST, ((DayInfoListAdapter) listView.getAdapter()).getItems());
    }
}