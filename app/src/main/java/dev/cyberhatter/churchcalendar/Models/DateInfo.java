package dev.cyberhatter.churchcalendar.Models;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class DateInfo implements Parcelable {

    String date;
    String season;
    Long season_week;
    String weekday;
    Celebration[] celebrations;

    private DateInfo(Parcel in) {
        date = in.readString();
        season = in.readString();
        if (in.readByte() == 0) {
            season_week = null;
        } else {
            season_week = in.readLong();
        }
        weekday = in.readString();
        int len = in.readInt();
        celebrations = new Celebration[len];
        for (int i=0; i<len; i++) {
            celebrations[i].title = in.readString();
            celebrations[i].colour = in.readString();
        }
    }

    public static final Creator<DateInfo> CREATOR = new Creator<DateInfo>() {
        @Override
        public DateInfo createFromParcel(Parcel in) {
            return new DateInfo(in);
        }

        @Override
        public DateInfo[] newArray(int size) {
            return new DateInfo[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(date);
        dest.writeString(season);
        if (season_week == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeLong(season_week);
        }
        dest.writeString(weekday);
        dest.writeInt(celebrations.length);
        for (Celebration celebration : celebrations) {
            dest.writeString(celebration.title);
            dest.writeString(celebration.colour);
        }
    }

    @NoArgsConstructor
    @Getter
    @Setter
    public class Celebration {
        String title;
        String colour;
        //String rank;
        //String rank_num;
    }
}