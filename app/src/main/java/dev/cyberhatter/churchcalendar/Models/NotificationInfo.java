package dev.cyberhatter.churchcalendar.Models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class NotificationInfo {
    int id;
    String text;
    Long date;
}
