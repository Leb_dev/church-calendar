package dev.cyberhatter.churchcalendar;

import android.content.Intent;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificationDialogActivity extends AppCompatActivity {

    @BindView(R.id.dialogText)
    EditText editText;
    @BindView(R.id.dialogDatePicker)
    DatePicker datePicker;
    @BindView(R.id.dialogTimePicker)
    TimePicker timePicker;
    int id = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_dialog);
        ButterKnife.bind(this);

        Calendar calendar = Calendar.getInstance();
        String notificationText = "";

        if (getIntent().hasExtra(NotificationsActivity.BUNDLE_NOTIFICATION_TEXT)) {
            notificationText = getIntent().getStringExtra(NotificationsActivity.BUNDLE_NOTIFICATION_TEXT);
        }
        if (getIntent().hasExtra(NotificationsActivity.BUNDLE_NOTIFICATION_DATE)) {
            calendar.setTimeInMillis(
                    getIntent().getLongExtra(
                            NotificationsActivity.BUNDLE_NOTIFICATION_DATE, calendar.getTimeInMillis()
                    )
            );
        }
        if (getIntent().hasExtra(NotificationsActivity.BUNDLE_NOTIFICATION_ID)) {
            id = getIntent().getIntExtra(NotificationsActivity.BUNDLE_NOTIFICATION_ID, -1);
        }

        editText.setText(notificationText + "");
        datePicker.init(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), null);
        timePicker.setIs24HourView(true);
        timePicker.setMinute(calendar.get(Calendar.MINUTE));
        timePicker.setHour(calendar.get(Calendar.HOUR_OF_DAY));
    }

    @OnClick(R.id.saveButton)
    void save() {
        Intent intent = new Intent();
        intent.putExtra(NotificationsActivity.BUNDLE_NOTIFICATION_TEXT, editText.getText().toString());
        Calendar calendar = Calendar.getInstance();
        calendar.set(datePicker.getYear(), datePicker.getMonth(), datePicker.getDayOfMonth(), timePicker.getHour(), timePicker.getMinute(), 0);
        intent.putExtra(NotificationsActivity.BUNDLE_NOTIFICATION_DATE, calendar.getTimeInMillis());
        if (id!=-1) {
            intent.putExtra(NotificationsActivity.BUNDLE_NOTIFICATION_ID, id);
        }
        setResult(RESULT_OK, intent);
        finish();
    }
}
