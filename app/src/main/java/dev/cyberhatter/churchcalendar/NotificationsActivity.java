package dev.cyberhatter.churchcalendar;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dev.cyberhatter.churchcalendar.Adapters.NotificationInfoListAdapter;
import dev.cyberhatter.churchcalendar.Database.DataBaseHelper;
import dev.cyberhatter.churchcalendar.Models.NotificationInfo;

import static dev.cyberhatter.churchcalendar.WakeUpHandler.REQUEST_CODE_SET_TIME;

public class NotificationsActivity extends AppCompatActivity {

    @BindView(R.id.notificationsList)
    ListView listView;
    public DataBaseHelper dataBaseHelper = new DataBaseHelper(this);
    public final int REQUEST_CODE_SET_UP_NOTIFICATION = 0;
    public final int REQUEST_CODE_EDIT_NOTIFICATION = 1;
    public static final String BUNDLE_NOTIFICATION_ID = "NotificationId";
    public static final String BUNDLE_NOTIFICATION_TEXT = "NotificationText";
    public static final String BUNDLE_NOTIFICATION_DATE = "NotificationDate";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        ButterKnife.bind(this);

        setAdapter();
    }

    @OnClick(R.id.addNotification)
    void addNotification() {
        startActivityForResult(new Intent(this, NotificationDialogActivity.class), REQUEST_CODE_SET_UP_NOTIFICATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null || resultCode != RESULT_OK) {
            return;
        }
        switch (requestCode) {
            default:
                break;
            case REQUEST_CODE_SET_UP_NOTIFICATION:
                if (data.hasExtra(BUNDLE_NOTIFICATION_TEXT)
                        && data.hasExtra(BUNDLE_NOTIFICATION_DATE)) {
                    NotificationInfo notificationInfo = dataBaseHelper.addNotification(
                            new NotificationInfo(
                                    0,
                                    data.getStringExtra(BUNDLE_NOTIFICATION_TEXT),
                                    data.getLongExtra(BUNDLE_NOTIFICATION_DATE, new Date().getTime())
                            )
                    );
                    if (notificationInfo != null) {
                        setAdapter();
                    }
                }
                break;
            case REQUEST_CODE_EDIT_NOTIFICATION:
                if (data.hasExtra(BUNDLE_NOTIFICATION_TEXT)
                        && data.hasExtra(BUNDLE_NOTIFICATION_DATE)
                        && data.hasExtra(BUNDLE_NOTIFICATION_ID)) {
                    dataBaseHelper.editNotification(
                            new NotificationInfo(
                                    data.getIntExtra(BUNDLE_NOTIFICATION_ID, -1),
                                    data.getStringExtra(BUNDLE_NOTIFICATION_TEXT),
                                    data.getLongExtra(BUNDLE_NOTIFICATION_DATE, new Date().getTime())
                            )
                    );
                    setAdapter();
                }
                break;
        }
    }

    @Override
    protected void onStop() {
        Intent intent = new Intent(this, WakeUpHandler.class);
        intent.setAction(getPackageName() + ".set_time");
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                this,
                REQUEST_CODE_SET_TIME,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        try {
            pendingIntent.send();
        } catch (PendingIntent.CanceledException e) {
            e.printStackTrace();
        }
        super.onStop();
    }

    public void setAdapter() {
        listView.setAdapter(
                new NotificationInfoListAdapter(
                        this,
                        dataBaseHelper.getAllNotificationInfos()
                )
        );
    }
}
