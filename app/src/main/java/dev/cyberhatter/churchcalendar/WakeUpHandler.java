package dev.cyberhatter.churchcalendar;

import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import java.util.ArrayList;
import java.util.Calendar;

import dev.cyberhatter.churchcalendar.Database.DataBaseHelper;
import dev.cyberhatter.churchcalendar.Models.NotificationInfo;

public class WakeUpHandler extends BroadcastReceiver {

    private final String BUNDLE_IDS = "ids";
    public static final int REQUEST_CODE_NOTIFY = 0;
    public static final int REQUEST_CODE_SET_TIME = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction() != null) {
            if (intent.getAction().equals(context.getPackageName() + ".notify")) {
                createNotification(context, intent);
            }
        }
        setNextWakeUpTime(context);
    }

    /**
     * Get the next wake up time and notification ids
     *
     * @param context context
     */
    private void setNextWakeUpTime(Context context) {
        ArrayList<NotificationInfo> notificationInfos = new DataBaseHelper(context).getNextWakeTime();
        if (notificationInfos.isEmpty()) {
            Intent intent = new Intent(context, WakeUpHandler.class);
            intent.setAction(context.getPackageName() + ".notify");
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context,
                    REQUEST_CODE_NOTIFY,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                alarmManager.cancel(pendingIntent);
            }
        } else {
            int[] ids = new int[notificationInfos.size()];
            for (int i = 0; i < notificationInfos.size(); i++) {
                ids[i] = notificationInfos.get(i).getId();
            }

            Intent intent = new Intent(context, WakeUpHandler.class);
            intent.setAction(context.getPackageName() + ".notify");
            intent.putExtra(BUNDLE_IDS, ids);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(
                    context,
                    REQUEST_CODE_NOTIFY,
                    intent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            );
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            if (alarmManager != null) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, notificationInfos.get(0).getDate(), pendingIntent);
            }
        }
    }

    /**
     * Create and show notification
     *
     * @param context context
     * @param intent  intent
     */
    private void createNotification(Context context, Intent intent) {
        int[] ids = intent.getIntArrayExtra(BUNDLE_IDS);
        DataBaseHelper dataBaseHelper = new DataBaseHelper(context);
        ArrayList<String> arrayList = dataBaseHelper.getNotifications(ids);
        for (int i=0; i<arrayList.size(); i++) {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                String NOTIFICATION_CHANNEL_ID = "ChurchCalendarChannelId";
                String NOTIFICATION_CHANNEL_NAME = "ChurchCalendarChannelName";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    NotificationChannel notificationChannel =
                            new NotificationChannel(
                                    NOTIFICATION_CHANNEL_ID,
                                    NOTIFICATION_CHANNEL_NAME,
                                    NotificationManager.IMPORTANCE_DEFAULT
                            );
                    notificationChannel.enableLights(true);
                    notificationChannel.setLightColor(Color.BLUE);
                    notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
                    notificationChannel.enableVibration(true);
                    notificationManager.createNotificationChannel(notificationChannel);
                }
                Notification notification =
                        new NotificationCompat.Builder(
                                context,
                                NOTIFICATION_CHANNEL_ID
                        )
                                .setSmallIcon(R.drawable.ic_launcher_foreground)
                                .setContentTitle("Church Calendar Notification")
                                .setContentText(arrayList.get(i))
                                .setWhen(Calendar.getInstance().getTimeInMillis())
                                .setAutoCancel(true)
                                .setPriority(1)
                                .build();
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                    notification.defaults =
                            notification.defaults |
                                    Notification.DEFAULT_LIGHTS |
                                    Notification.DEFAULT_VIBRATE |
                                    Notification.DEFAULT_SOUND;
                }
                notification.flags =
                        notification.flags |
                                Notification.VISIBILITY_PUBLIC;
                notificationManager.notify(ids[i], notification);
            }
        }
    }
}
